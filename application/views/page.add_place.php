<?php $this->load->view('block.header.php'); ?>
    <section id="content">
      <section class="vbox">
        <section class="scrollable padder">
              <div class="m-b-md">
                <h3 class="m-b-none">Place Form</h3>
              </div>
              <div class="row">
                
                <div class="col-sm-12">
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">Place form</header>
                    <div class="panel-body">
                        <form action="<?= site_url('place/add_place') ?>" class="bs-example form-horizontal" method="post" id="placeForm">
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Title *</label>
                                <div class="col-lg-10">
                                    <input type="text" name="title" class="form-control" placeholder="Title">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Description *</label>
                                <div class="col-lg-10">
                                    <textarea class="form-control" name="description"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Latitude *</label>
                                <div class="col-lg-10">
                                    <input type="text" name="latitude" class="form-control" placeholder="Latitude">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Longitude *</label>
                                <div class="col-lg-10">
                                    <input type="text" name="longitude" class="form-control" placeholder="Longitude">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Rating *</label>
                                <div class="col-lg-10">
                                    <input type="text" name="rating" class="form-control" placeholder="Rating">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Image *</label>
                                <div class="col-lg-10">
                                    <textarea class="form-control" name="image"></textarea>
                                    <span class="help-block m-b-none">Images url, If multiple use comma</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                  </section>
                </div>
              </div>
              
            </section>
      </section>
      <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a>
    </section>
<script type="text/javascript">
    $(document).ready(function (e) {
        $("#placeForm").submit(function (e) {
            e.preventDefault();
            var str = $("#placeForm").serialize();
            $.ajax({
                type: "POST",
                url: "<?= site_url('place/place_detail') ?>",
                data: str,
                dataType: "JSON",
                success: function (res) {
                    if(res.status == true && res.code == '200')
                    {
                         alertify.alert(res.message).set('onok', function(){
                             window.location.href = '<?= site_url('place'); ?>';
                         }); 
                    }
                    else if(res.status == false && res.code == '400')
                    {
                        var errors = '';
                        $.each(res.validation_error, function (index, val) {
                                errors += val + '<br />';
                        });
                        alertify.alert(errors);
                    }
                    else
                    {
                        alertify.alert(res.message);
                    }
                },
                error: function(res) {
                    alertify.alert(res.message);
                }
            });
        });
    });
</script>

<?php $this->load->view('block.footer.php'); ?>
