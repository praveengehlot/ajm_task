<!DOCTYPE html>
<html lang="en" class="app">
<head>
  <meta charset="utf-8" />
  <title>Place</title>
  <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /> 
  <link rel="stylesheet" href="<?= site_url()?>/assets/css/bootstrap.css" type="text/css" />
  <link rel="stylesheet" href="<?= site_url()?>/assets/css/animate.css" type="text/css" />
  <link rel="stylesheet" href="<?= site_url()?>/assets/css/font-awesome.min.css" type="text/css" />
  <link rel="stylesheet" href="<?= site_url()?>/assets/css/font.css" type="text/css" />
  <link rel="stylesheet" href="<?= site_url()?>/assets/js/datatables/datatables.css" type="text/css"/>
  <link rel="stylesheet" href="<?= site_url()?>/assets/css/app.css" type="text/css" />
  <link rel="stylesheet" href="<?= site_url("/assets/js/alertifyjs/css/alertify.css") ?>" type="text/css" />
  <script src="<?= site_url()?>/assets/js/jquery-3.3.1.js"></script>
  <style type="text/css">
    .morecontent span 
    {
        display: none;
    }
</style>
  <!--[if lt IE 9]>
    <script src="js/ie/html5shiv.js"></script>
    <script src="js/ie/respond.min.js"></script>
    <script src="js/ie/excanvas.js"></script>
  <![endif]-->
</head>
<body class="">
  <section class="vbox">
    <header class="bg-dark dk header navbar navbar-fixed-top-xs">
      <div class="navbar-header aside-md">
        <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen,open" data-target="#nav,html">
          <i class="fa fa-bars"></i>
        </a>
          <a href="<?= site_url('place') ?>" class="navbar-brand"><img src="<?= site_url(); ?>assets/images/logo.png" class="m-r-sm">Place</a>
        <a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".nav-user">
          <i class="fa fa-cog"></i>
        </a>
      </div>   
    </header>
    <section>
      <section class="hbox stretch">
       