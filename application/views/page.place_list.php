<?php $this->load->view('block.header.php'); ?>
        <script type="text/javascript">
        $(document).ready(function() {
            
            if ("geolocation" in navigator){
                    navigator.geolocation.getCurrentPosition(function(position){ 
                        $('#latitude').val(position.coords.latitude);
                        $('#longitude').val(position.coords.longitude);
                    });
            }else{
                    console.log("Browser doesn't support geolocation!");
            } 
            
            var oTable = $("#Place_Datatable").DataTable({
            "processing": true,
            "stateSave": true,
            "ajax" : {
                "url": "<?= site_url('place/place_detail')?>",
                "data": function (d) {
                    d.area = $('#oTable_area_filter').val();
                    d.latitude = $('#latitude').val();
                    d.longitude = $('#longitude').val();
                },
                "error": function(xhr, error, thrown) {
                    if (xhr.status != '410') {
//                        alertify.alert(xhr.responseJSON.message);
                    }
                }
            },
            "fnDrawCallback": function( oSettings ) {
                var showChar = 50;
                var ellipsestext = "...";
                var moretext = "More";
                var lesstext = "Less";
                $('.moreactivity_description').each(function() {
                    var content = $(this).html();
                    if(content.indexOf('moreelipses') != -1){
                        content = $(this).find(".order_data").html();
                    }
                    if(content.length > showChar) 
                    {
                        var c = content.substr(0, showChar);
                        var h = content.substr(showChar, content.length - showChar);

                        var html = c + '<span class="moreelipses">'+ellipsestext+'</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span><span class="order_data hidden">'+content+'</span>';
                        $(this).html(html);
                    }
                });
                
                  $(".morelink").click(function(){
                    if($(this).hasClass("less")) {
                        $(this).removeClass("less");
                        $(this).html(moretext);
                    } else {
                        $(this).addClass("less");
                        $(this).html(lesstext);
                    }
                    $(this).parent().prev().toggle();
                    $(this).prev().toggle();
                    return false;
                });
                
            },
        });
            
        $('#table_filter_button').click(function (e) {
            e.preventDefault();
            oTable.ajax.reload();
        });
            
        $("#add_place_button").click(function (e) {
            e.preventDefault();
            $("#place_form_model").modal("show");
        });
        
        $("#placeForm").submit(function (e) {
            e.preventDefault();
            var str = $("#placeForm").serialize();
            $.ajax({
                type: "POST",
                url: "<?= site_url('place/place_detail') ?>",
                data: str,
                dataType: "JSON",
                success: function (res) {
                    if(res.status == true && res.code == '200')
                    {
                         alertify.alert(res.message).set('onok', function(){
                            $("#place_form_reset_button").trigger('click');
                            $('.modal').modal('hide');
                            $(".modal-backdrop").remove();
                            oTable.ajax.reload();
                         }); 
                    }
                    else if(res.status == false && res.code == '400')
                    {
                        var errors = '';
                        $.each(res.validation_error, function (index, val) {
                                errors += val + '<br />';
                        });
                        alertify.alert(errors);
                    }
                    else
                    {
                        alertify.alert(res.message);
                    }
                },
                error: function(res) {
                    alertify.alert(res.message);
                }
            });
        });
            
        });
        </script>
   
<script>

</script>
    <section id="content">
      <section class="vbox">
        <section class="scrollable padder">
          <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
            <li class="active">Place</li>
          </ul>
            <div class="m-b-md">
                <div class="row">
                    <label class="col-md-4"><a href="#" id="add_place_button" class="btn btn-s-md btn-info">Add Place</a></label>
                    <span class="col-md-3">
                        <select class="form-control" id="oTable_area_filter">
                            <option value="all">All</option>
                            <option value="1">1 Km</option>
                            <option value="2">2 Km</option>
                            <option value="5">5 Km</option>
                            <option value="10">10 Km</option>
                            <option value="20">20 Km</option>
                        </select>
                        
                        
                    </span>
                    <span class="col-md-2"><input type="text" class="form-control" name="latitude" placeholder="Latitude" id="latitude" value=""></span>
                    <span class="col-md-2"><input type="text" class="form-control" name="longitude" placeholder="Longitude" id="longitude" value=""></span>
                    <span class="col-md-1"><input type="button" id="table_filter_button" class="btn btn-primary" value="Filter"></span>
                </div>
            </div>
          <section class="panel panel-default">
            <header class="panel-heading">
              Place 
              <i class="fa fa-info-sign text-muted" data-toggle="tooltip" data-placement="bottom" data-title="ajax to load the data."></i> 
            </header>
            <div class="table-responsive">
                <table class="table table-striped m-b-none" id="Place_Datatable" data-ride="datatables">
                <thead>
                  <tr>
                    <th width="20%">Title</th>
                    <th width="35%">Description</th>
                    <th width="15%">Latitude</th>
                    <th width="15%">Longitude</th>
                    <th width="15%">Rating</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </section>
        </section>
      </section>
      <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a>
    </section>


<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="placeFormLabel" aria-hidden="true" id="place_form_model">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Place</h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <form action="<?= site_url('place/add_place') ?>" class="bs-example form-horizontal" method="post" id="placeForm">
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Title *</label>
                            <div class="col-lg-10">
                                <input type="text" name="title" class="form-control" placeholder="Title">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Description *</label>
                            <div class="col-lg-10">
                                <textarea class="form-control" name="description"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Latitude *</label>
                            <div class="col-lg-10">
                                <input type="text" name="latitude" class="form-control" placeholder="Latitude">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Longitude *</label>
                            <div class="col-lg-10">
                                <input type="text" name="longitude" class="form-control" placeholder="Longitude">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Rating *</label>
                            <div class="col-lg-10">
                                <input type="text" name="rating" class="form-control" placeholder="Rating">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Image *</label>
                            <div class="col-lg-10">
                                <textarea class="form-control" name="image"></textarea>
                                <span class="help-block m-b-none">Images url, If multiple use comma</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                                <button type="reset" class="btn btn-sm btn-default" id="place_form_reset_button">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<?php $this->load->view('block.footer.php'); ?>