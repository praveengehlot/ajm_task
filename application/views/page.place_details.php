<?php $this->load->view('block.header.php'); ?>
<style type="text/css">
    #gallery .container-fluid .col-md-3 img {
    margin-top: 30px;
}

#gallery {
    margin: 0 auto;
    background-size: cover;
    background-repeat: no-repeat;
    height: auto;
    width: 100%;
}
    
</style>
<script type="text/javascript">
        var to_latitude = '<?= $location['latitude']?>';
        var to_longitude = '<?= $location['longitude']?>';
        var place_detail_url = '<?= site_url('place/get_distance') ?>';
            $(document).ready(function() {
                 if ("geolocation" in navigator){ //check geolocation available 
                    //try to get user current location using getCurrentPosition() method
                    navigator.geolocation.getCurrentPosition(function(position){ 
                    var from_latitude = position.coords.latitude;
                    var from_longitude = position.coords.longitude;
                    $.ajax({
                        type: "GET",
                        url: place_detail_url,
                        data: {from_latitude:from_latitude, from_longitude:from_longitude, to_latitude:to_latitude, to_longitude:to_longitude},
                        dataType: "JSON",
                        success: function (res) {
                            if (res.status == true && res.code == '200')
                            {
                                $("#distance").html(res.distance+' Km');
                            } 
                            else
                            {
                                $("#distance").html('--');
                            }
                        },
                        error: function (res) {
                            $("#distance").html('--');
                        }
                    });
                });
            } else {
                console.log("Browser doesn't support geolocation!");
            }

        });
</script>
<section id="content">
      <section class="vbox">
        <header class="header bg-white b-b b-light">
            <p><?= $title?></p>
            <p class="pull-right" style="margin-top: 8px"><a href="<?= site_url('place'); ?>" class="btn btn-default"> Back</a></p>
        </header>
        <section class="scrollable">
          <section class="hbox stretch">
            <aside class="aside-lg bg-light lter b-r">
              <section class="vbox">
                <section class="scrollable">
                  <div class="wrapper">
                    <div class="clearfix m-b">
                      <div class="clear">
                        <div class="h3 m-t-xs m-b-xs"><?= $title?></div>
                      </div>                
                    </div>
                    <div>
                      <small class="text-uc text-xs text-muted">rating</small>
                      <p><?= $rating?></p>
                      <small class="text-uc text-xs text-muted">description</small>
                      <p><?= $description?></p>
                      <small class="text-uc text-xs text-muted">Distance</small>
                      <p id="distance">--</p>
                      <small class="text-uc text-xs text-muted">Gallery</small>

                      <section id="gallery">
                          <!--all images-->
                          <div class="container-fluid mixitup">
                            <?php
                            foreach ($images as $img)
                            {
                            ?>
                                <div class="col-md-3 col-sm-3 col-lg-3">
                                    <img src="<?= $img ?>" alt="image1" class="img-responsive">
                                </div>
                            <?php
                            }
                            ?>
                          </div>
                      </section>
                      <div class="line"></div>

                    </div>
                  </div>
                </section>
              </section>
            </aside>
          </section>
        </section>
      </section>
      <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a>
    </section>
<script src="<?= site_url("/assets/js/place/place_details.js") ?>"></script>
<?php $this->load->view('block.footer.php'); ?>
