
      </section>
    </section>
  </section>
    
  <script src="<?= site_url()?>/assets/js/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?= site_url()?>/assets/js/bootstrap.js"></script>
    <!-- App -->
    <script src="<?= site_url()?>/assets/js/app.js"></script> 
    <script src="<?= site_url()?>/assets/js/slimscroll/jquery.slimscroll.min.js"></script>
    <!-- datatables -->
    <script src="<?= site_url()?>/assets/js/datatables/jquery.dataTables.min.js"></script>
    <script src="<?= site_url()?>/assets/js/app.plugin.js"></script>
    
    <script src="<?= site_url("/assets/js/alertifyjs/alertify.js") ?>"></script>
    <script src="<?= site_url("/assets/js/alertifyjs/alertify.min.js") ?>"></script>
    <script src="<?= site_url("/assets/js/pjax/app.js") ?>"></script>
    
     <script>
            //override defaults
            alertify.defaults.glossary.title = "Notification";
            alertify.defaults.theme.ok = "btn btn-sm btn-primary";
            alertify.defaults.theme.cancel = "btn btn-sm btn-default";
        </script>
</body>
</html>