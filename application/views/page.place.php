<?php
$this->load->view('block.header.php');
?>

<script type="text/javascript">

var place_url = '<?= site_url('api/places/place')?>';


    $(document).ready(function(e) {
        
            var PlaceTable1KM = $("#Place_Datatable_In_1_Km").DataTable({
                "processing": true,
                "stateSave": true,
                "searching": false,
                "ajax" : {
                    "url": place_url,
                    "data": function (d) {
                        d.area = 1;
                        d.latitude = $('#latitude').val();
                        d.longitude = $('#longitude').val();
                    },
                    "error": function(xhr, error, thrown) {
                        if (xhr.status != '410') {}
                    }
                },
            });
        
            var PlaceTable2KM = $("#Place_Datatable_In_2_Km").DataTable({
                "processing": true,
                "stateSave": true,
                "searching": false,
                "ajax" : {
                    "url": place_url,
                    "data": function (d) {
                        d.area = 2;
                        d.latitude = $('#latitude').val();
                        d.longitude = $('#longitude').val();
                    },
                    "error": function(xhr, error, thrown) {
                        if (xhr.status != '410') {}
                    }
                },
            });
        
            var PlaceTable5KM = $("#Place_Datatable_In_5_Km").DataTable({
                "processing": true,
                "stateSave": true,
                "searching": false,
                "ajax" : {
                    "url": place_url,
                    "data": function (d) {
                        d.area = 5;
                        d.latitude = $('#latitude').val();
                        d.longitude = $('#longitude').val();
                    },
                    "error": function(xhr, error, thrown) {
                        if (xhr.status != '410') {}
                    }
                },
            });
            var PlaceTable10KM = $("#Place_Datatable_In_10_Km").DataTable({
                "processing": true,
                "stateSave": true,
                "searching": false,
                "ajax" : {
                    "url": place_url,
                    "data": function (d) {
                        d.area = 10;
                        d.latitude = $('#latitude').val();
                        d.longitude = $('#longitude').val();
                    },
                    "error": function(xhr, error, thrown) {
                        if (xhr.status != '410') {}
                    }
                },
            });
        
            var PlaceTable20KM = $("#Place_Datatable_In_20_Km").DataTable({
                "processing": true,
                "stateSave": true,
                "searching": false,
                "ajax" : {
                    "url": place_url,
                    "data": function (d) {
                        d.area = 20;
                        d.latitude = $('#latitude').val();
                        d.longitude = $('#longitude').val();
                    },
                    "error": function(xhr, error, thrown) {
                        if (xhr.status != '410') {}
                    }
                },
            });
        
            var PlaceTable40KM = $("#Place_Datatable_In_40_Km").DataTable({
                "processing": true,
                "stateSave": true,
                "searching": false,
                "ajax" : {
                    "url": place_url,
                    "data": function (d) {
                        d.area = 40;
                        d.latitude = $('#latitude').val();
                        d.longitude = $('#longitude').val();
                    },
                    "error": function(xhr, error, thrown) {
                        if (xhr.status != '410') {}
                    }
                },
            });
    
        
            if ("geolocation" in navigator){
                navigator.geolocation.getCurrentPosition(function(position){ 
                    $('#latitude').val(position.coords.latitude);
                    $('#longitude').val(position.coords.longitude);
                    PlaceTable1KM.ajax.reload();
                    PlaceTable2KM.ajax.reload();
                    PlaceTable5KM.ajax.reload();
                    PlaceTable10KM.ajax.reload();
                    PlaceTable20KM.ajax.reload();
                    PlaceTable40KM.ajax.reload();
                });
            }
            else
            {       
                console.log("Browser doesn't support geolocation!");
            } 
        
             
        $("#add_place_button").click(function (e) {
            e.preventDefault();
            $("#place_form_model").modal("show");
        });
        
        $("#placeForm").submit(function (e) {
            e.preventDefault();
            var str = $("#placeForm").serialize();
            $.ajax({
                type: "POST",
                url: place_url,
                data: str,
                dataType: "JSON",
                success: function (res) {
                    
                    if(res.status == true && res.code == '200')
                    {
                         alertify.alert(res.message).set('onok', function(){
                            $("#place_form_reset_button").trigger('click');
                            $('.modal').modal('hide');
                            $(".modal-backdrop").remove();
                            PlaceTable1KM.ajax.reload();
                            PlaceTable2KM.ajax.reload();
                            PlaceTable5KM.ajax.reload();
                            PlaceTable10KM.ajax.reload();
                            PlaceTable20KM.ajax.reload();
                            PlaceTable40KM.ajax.reload();
                         }); 
                    }
                    else if(res.status == false && res.code == '400')
                    {
                        var errors = '';
                        $.each(res.validation_error, function (index, val) {
                                errors += val + '<br />';
                        });
                        alertify.alert(errors);
                    }
                    else
                    {
                        alertify.alert(res.message);
                    }
                },
                error: function(result) {
                    var res = result.responseJSON;
                    if(res.status == false && res.code == '400')
                    {
                        var errors = '';
                        $.each(res.validation_error, function (index, val) {
                                errors += val + '<br />';
                        });
                        alertify.alert(errors);
                    }
                    else
                    {
                        alertify.alert(res.message);
                    }
                }
            });
        });
       
            
    });
</script>
<section id="content">
    <section class="vbox">
        <header class="header bg-light bg-gradient b-b">
            <p>List Places</p>
        </header>
        <section class="scrollable wrapper" id="scrollable">
            <div class="m-b-md">
                <div class="row">
                    <label class="col-md-4"><a href="#" id="add_place_button" class="btn btn-s-md btn-info">Add Place</a></label>
                    <input type="hidden" name="latitude" id="latitude" value="">
                    <input type="hidden" name="longitude" id="longitude" value="">

                </div>
            </div>
            <div class="row">            
                <div class="col-sm-6">
                    <section class="panel panel-default">
                        <header class="panel-heading">
                            <div class="input-group text-sm">
                                <b>Place in 1 Km</b>
                            </div>
                        </header>
                        <div class="table-responsive">
                            <table class="table table-striped m-b-none" id="Place_Datatable_In_1_Km" data-ride="datatables">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Distance</th>
                                        <th>Image</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </section>          
                </div>
                <div class="col-sm-6">
                    <section class="panel panel-default">
                        <header class="panel-heading">
                            <div class="input-group text-sm">
                                <b>Place in 2 Km</b>
                            </div>
                        </header>
                        <div class="table-responsive">
                            <table class="table table-striped m-b-none" id="Place_Datatable_In_2_Km" data-ride="datatables">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Distance</th>
                                        <th>Image</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </section>          
                </div>
                <div class="col-sm-6">
                    <section class="panel panel-default">
                        <header class="panel-heading">
                            <div class="input-group text-sm">
                                <b>Place in 5 Km</b>
                            </div>
                        </header>
                        <div class="table-responsive">
                            <table class="table table-striped m-b-none" id="Place_Datatable_In_5_Km" data-ride="datatables">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Distance</th>
                                        <th>Image</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </section>          
                </div>
                <div class="col-sm-6">
                    <section class="panel panel-default">
                        <header class="panel-heading">
                            <div class="input-group text-sm">
                                <b>Place in 10 Km</b>
                            </div>
                        </header>
                        <div class="table-responsive">
                            <table class="table table-striped m-b-none" id="Place_Datatable_In_10_Km" data-ride="datatables">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Distance</th>
                                        <th>Image</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </section>          
                </div>
                <div class="col-sm-6">
                    <section class="panel panel-default">
                        <header class="panel-heading">
                            <div class="input-group text-sm">
                                <b>Place in 20 Km</b>
                            </div>
                        </header>
                        <div class="table-responsive">
                            <table class="table table-striped m-b-none" id="Place_Datatable_In_20_Km" data-ride="datatables">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Distance</th>
                                        <th>Image</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </section>          
                </div>
                <div class="col-sm-6">
                    <section class="panel panel-default">
                        <header class="panel-heading">
                            <div class="input-group text-sm">
                                <b>Place in 40+ Km</b>
                            </div>
                        </header>
                        <div class="table-responsive">
                            <table class="table table-striped m-b-none" id="Place_Datatable_In_40_Km" data-ride="datatables">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Distance</th>
                                        <th>Image</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </section>          
                </div>
            </div>
        </section>
    </section>
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a>
</section>
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="placeFormLabel" aria-hidden="true" id="place_form_model">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Place</h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <form action="<?= site_url('place/add_place') ?>" class="bs-example form-horizontal" method="post" id="placeForm">
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Title *</label>
                            <div class="col-lg-10">
                                <input type="text" name="title" class="form-control" placeholder="Title">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Description *</label>
                            <div class="col-lg-10">
                                <textarea class="form-control" name="description"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Latitude *</label>
                            <div class="col-lg-10">
                                <input type="text" name="latitude" class="form-control" placeholder="Latitude">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Longitude *</label>
                            <div class="col-lg-10">
                                <input type="text" name="longitude" class="form-control" placeholder="Longitude">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Rating *</label>
                            <div class="col-lg-10">
                                <input type="text" name="rating" class="form-control" placeholder="Rating">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Image *</label>
                            <div class="col-lg-10">
                                <textarea class="form-control" name="image"></textarea>
                                <span class="help-block m-b-none">Images url, If multiple use comma</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                                <button type="reset" class="btn btn-sm btn-default" id="place_form_reset_button">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->load->view('block.footer.php');
?>