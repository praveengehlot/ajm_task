<?php 

require (APPPATH.'libraries/REST_Controller.php');

class Places extends REST_Controller {

    public function __construct()
    {
        parent::__construct();
    }
    
     /**
     * Place Create POST
     *
     * Create a new place and respond with a status or error.
     *
     * @param array $data the user details
     * @return void
     */
    public function place_post()
    {
        $this->form_validation->set_data($this->input->post());
        if ($this->form_validation->run('place_create_UI') == FALSE)
        {
            $errors = $this->form_validation->error_array();
            $result = array('code' => 400,'status' => false, 'validation_error' => $errors);
            $this->response($result, 400);
        }
        else
        {
            $postdata = $this->input->post();
            $data = array();
            $data['title'] = $postdata['title'];
            $data['description'] = $postdata['description'];
            $images = $postdata['image'];
            $data['images'] = explode(",",$images);
            $data['location'] = array('latitude' => $postdata['latitude'], 'longitude' => $postdata['longitude']);
            $data['rating'] = $postdata['rating'];
            $data['id'] = generateRandomString();

            $filedata = file_get_contents('data.json');
            $tempArray = json_decode($filedata);
            array_push($tempArray, $data);
            $jsonData = json_encode($tempArray);
            if(file_put_contents('data.json', $jsonData))
            {
                $result = array('status' => TRUE, 'code'=>200, 'message' => 'Place created successfully');
                $this->response($result, 200);
            }
            else 
            {
                $result = array('status' => FALSE, 'code'=>500, 'message' => 'Place not created');
                $this->response($result, 500);
            }
        }

    }
    
    /**
     * List GET
     *
     * Responds with information about a user.
     *
     * @param string $id the ID of the user
     * @return void
     */
    public function place_get()
    {
        $area = $this->input->get('area');
        $latitude = $this->input->get('latitude');
        $longitude = $this->input->get('longitude');
//        $latitude = 26.29894;
//        $longitude = 73.021523;
        
        // get data from file
        $str = file_get_contents('data.json');
        $jsondecode = json_decode($str, TRUE);
        
        $return_arr = array();
        if (!empty($jsondecode))
        {
            foreach ($jsondecode as $val)
            {
                $res = array();
                if (!empty($area) && !empty($longitude) && !empty($latitude))
                {
                    $current_lat = $latitude;
                    $current_long = $longitude;
                    $user_lat = $val['location']['latitude'];
                    $user_long = $val['location']['longitude'];
                    $distance = get_distance_between_lat_long($current_lat, $current_long, $user_lat, $user_long);
                    $diffrence_distance = ceil($distance / 1000); // convert into Km
                    
                    if ($area <= 20)
                    {
                        if ($diffrence_distance <= $area)
                        {
                            $res[] = '<a href="' . site_url('place/view_details/') . $val['id'] . '">' . $val['title'] . '</a>';
                            $res[] = $diffrence_distance . ' Km';
                            
                            $imgHtml = '';
                            foreach ($val['images'] as $img)
                            {
                                if($img)
                                $imgHtml .= '<div class="col-md-3 col-sm-3 col-lg-3" style="padding-right:0px;"><img src="'. $img .'" alt="image1" class="img-thumbnail"></div>';
                            }
                            
                            $res[] = $imgHtml;
                            $return_arr[] = $res;
                        }
                    }
                    else
                    {
                        if ($diffrence_distance >= $area)
                        {
                            $res[] = '<a href="' . site_url('place/view_details/') . $val['id'] . '">' . $val['title'] . '</a>';
                            $res[] = $diffrence_distance . ' Km';
                            
                            $imgHtml = '';
                            foreach ($val['images'] as $img)
                            {
                                if($img)
                                $imgHtml .= '<div class="col-md-3 col-sm-3 col-lg-3" style="padding-right:0px;"><img src="'. $img .'" alt="image1" class="img-thumbnail"></div>';
                            }
                            
                            $res[] = $imgHtml;
                            $return_arr[] = $res;
                        }
                    }
                }
                else
                {
                    $res[] = '<a href="' . site_url('place/view_details/') . $val['id'] . '">' . $val['title'] . '</a>';
                    $res[] = '--';

                    $imgHtml = '';
                    foreach ($val['images'] as $img)
                    {
                        if($img)
                        $imgHtml .= '<div class="col-md-3 col-sm-3 col-lg-3" style="padding-right:0px;"><img src="'. $img .'" alt="image1" class="img-thumbnail"></div>';
                    }

                    $res[] = $imgHtml;
                    $return_arr[] = $res;
                }
            }
        }
        $result['aaData'] = $return_arr;

        $this->response($result, 200);
    }

}