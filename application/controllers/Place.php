<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Place extends CI_Controller
{

    public function index()
    {
        $this->load->view('page.place.php');
    }

    public function view_details()
    {
        $place_id = $this->uri->segment(3);

        $str = file_get_contents('data.json');
        $jsondecode = json_decode($str, true); // decode the JSON into an associative array
        
        $place_details_data = '';
        foreach ($jsondecode as $key => $val)
        {
            if ($val['id'] === $place_id) {
                $place_details_data = $val;
            }
        }
     
        if(empty($place_details_data))
        {
            redirect(site_url('place'), 'location');
        }
        
        
//        p($place_details_data);
        $this->load->view('page.place_details.php', $place_details_data);
    }
    
    public function get_distance()
    {
        $get_data = $this->input->get();
        if(!empty($get_data))
        {
            $from_latitude = $get_data['from_latitude'];
            $from_longitude= $get_data['from_longitude'];
            $to_latitude = $get_data['to_latitude'];
            $to_longitude= $get_data['to_longitude'];
            
            $distance = get_distance_between_lat_long($from_latitude, $from_longitude, $to_latitude, $to_longitude);
            $diffrence_distance = ceil($distance / 1000); // convert into Km
            
            $result = array('code' => 200,'status' => TRUE, 'distance' => $diffrence_distance);
            echo json_encode($result);
            exit();         
        }
        else
        {
            $result = array('code' => 200,'status' => TRUE, 'distance' => '--');
            echo json_encode($result);
            exit(); 
        }
    }
}
