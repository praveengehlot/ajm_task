<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This config file contains all form validation rules for all forms in the web application.
 */

$config = array(
    "place_create_UI" => array(
        array(
            'field' => 'title',
            'label' => 'lang:Title',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'description',
            'label' => 'lang:Description',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'latitude',
            'label' => 'lang:Latitude',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'longitude',
            'label' => 'lang:Longitude',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'rating',
            'label' => 'lang:Rating',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'image',
            'label' => 'lang:Image',
            'rules' => 'trim|required'
        ),
    ),
    
);