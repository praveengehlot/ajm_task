<?php
/**
 * API Base Controller
 *
 * @author		Filip Heitbrink
 * @copyright	Copyright (c) 2016, Ipsum Works B.V.
 * @version		Git: $Id$
 * @since		v1.0
 * @link		http://www.ipsumworks.com
 */

/**
 * API Base Controller Class
 *
 * This base class contains functions that are shared among the controllers under application/controllers/api.
 *
 * @package QBD\API
 */
class API_Base_Controller extends REST_Controller
{
	public function __construct()
	{
        parent::__construct();
		$this->_registerExceptionHandler();
	}

	/*
	 * Register Exception Handler
	 *
	 * Catch Exceptions and write them to the system log in the database.
	 * Shows default error page in the front-end.
	 *
	 * @access private
	 * @return void
	 */
	private function _registerExceptionHandler()
	{
		if ( ! ini_get('display_errors'))
		{
			$CI = $this; // $this can be used in anonymous functions automatically only from PHP 5.4
			set_exception_handler(function($exception) use ($CI)
			{
				try
				{
					$CI->load->model('Log_model');
					$CI->log_model->add(Log_model::LOG_TYPE_ERROR, $exception->getMessage(), true, $exception);
					redirect('home/error');
				}
				catch (Exception $e)
				{
					exit('The system is currently unavailable');
				}
			});
		}
	}

    protected function user_create()
    {
        try
        {
            //check user type and apply validation acording to user type
            $user_type = $this->post("user_type");

            // validate the fields
            if($user_type == USER_TYPE_LDAP)
            {
                $run_validation = 'user_create_LDAP_UI';
                if (defined('ACCESSED_VIA_API') && ACCESSED_VIA_API === TRUE)
                    $run_validation = 'user_create_LDAP_API';
            }
            else
            {
                $run_validation = 'user_create_LOCAL_UI';
                if (defined('ACCESSED_VIA_API') && ACCESSED_VIA_API === TRUE)
                    $run_validation = 'user_create_LOCAL_API';
            }
            
            $this->form_validation->set_data($this->post());
            if ($this->form_validation->run($run_validation) == FALSE)
            {
                $errors = $this->form_validation->error_array();
                $result = array('status' => false, 'validation_error' => $errors);
                $this->response($result, 400);
            }
            else
            {
                // special validations
                $errors = array();

                // check ldap_user_id contain domain if not then add
                $this->load->config("ldap");
                $ldap_config = $this->config->item('ldap_config');
                $default = ENVIRONMENT;
                $domain = $ldap_config[$default]['account_suffix'];

                if (strpos($this->post('ldap_user_id'), '@') === false)
                {
                    // user enter only user name 
                    $ldap_user_id = $this->post('ldap_user_id') . $domain;
                }
                else
                {
                    $ldap_user_id = $this->post('ldap_user_id');
                }


                // check if ldap_user_id already exists
                if (($user_id = $this->users_model->check_ldap_user_id_exists($ldap_user_id)) !== FALSE)
                {
                    $errors['email'] = lang('error_ldap_user_id_already_exists');
                    $result = array('status' => false, 'validation_error' => $errors);
                    $this->response($result, 400);
                }

                // check if allowed to assign this role
                $roles = $this->post('roles');
                $auth_role = $this->roles_model->check_role_assign($roles);
                if ($auth_role !== TRUE)
                {
                    // write to system log
                    $this->log_model->add('security', 'Attempted to assign an unauthorized permission role: ' . $auth_role['role']);
                    $errors['roles'] = lang('msg_not_authorized_to_assign_role') . ": " . lang($auth_role['role']);
                }

                // check if allowed to assign groups
                $groups = $this->post('groups');
                $auth_group = $this->groups_model->check_group_assign($groups);
                if ($auth_group !== TRUE)
                {
                    // write to system log
                    $this->log_model->add('security', 'Attempted to assign an unauthorized group: ' . $auth_group['group']);
                    $errors['roles'] = lang('msg_not_authorized_to_assign_group') . ": " . lang($auth_group['group']);
                }

                if (!empty($errors))
                {
                    // return validation error
                    $result = array('status' => false, 'validation_error' => $errors);
                    $this->response($result, 400);
                }
                else
                {
                    // start saving everything to the database
                    $data = $this->form_validation->get_prepped_data($this->post());
                    $data['ldap_user_id'] = $ldap_user_id;
                    
                    //if user type is local set mail with password
                    if ($data['user_type'] == USER_TYPE_LOCAL)
                    {
                    // reset the password, to be able to send it by email
                    $temporary_password = $this->auth_model->generate_temporary_password();
                    $data['password'] = $temporary_password;
                    // by default, require new users to reset their password when they first log in
                    $data['force_reset_password'] = '1';
                    }
                    else
                    {
                        $data['force_reset_password'] = '0';
                    }

                    // start database transaction
                    $this->db->trans_start();

                    // save user
                    unset($data['X-API-KEY']);
                    unset($data['user_id']);
                    unset($data['roles']);
                    unset($data['groups']);
                    unset($data['user_functions']);
                    unset($data['send_access_details']);
                    
                    // generate random id
                    $Tusers = $this->users_model->_table_name;
                    $data['user_random_id'] = $this->mydb->generate_random_id($Tusers, 'user_random_id');
                    
                    $id = $this->users_model->user_save(FALSE, $data);

                    // assign roles
                    $roles = $this->post('roles');
                    $this->roles_model->assign_roles($id, $roles);

                    // assign groups
                    $groups = $this->post('groups');
                    $this->groups_model->assign_groups($id, $groups);
                    
                    //assign user functions
                    $user_functions = $this->post('user_functions');
                    $this->user_functions_model->assign_users_user_functions($id, $user_functions);

                    $this->db->trans_complete();

                    if ($this->db->trans_status())
                    {
                        // write to system log
                        $this->log_model->add('write', 'New user created: ' . $ldap_user_id . ' (' . $id . ')');

                        $message = lang('msg_user_created');
                        
                        if ($data['user_type'] == USER_TYPE_LOCAL)
                        {
                            if ($this->email_notifications_model->send_access_details($id, $temporary_password))
                            {
                                // write to system log
                                $this->log_model->add('info', 'Email with access details sent to: ' . $data['email'] . ' (' . $id . ')');
                                $message .= ' ' . lang('msg_email_sent');
                            }
                            else
                            {
                                // write to system log
                                $this->log_model->add('error', 'Could not send email with access details to: ' . $data['email'] . ' (' . $id . ')');
                                $message .= ' ' . lang('msg_email_not_sent');
                            }

                        }

                        $result = array('status' => true, 'id' => $data['user_random_id'], 'message' => $message);
                        $this->response($result, 201);
                    }
                    else
                    {
                        // write to system log
                        $this->log_model->add('error', 'Could not create new user');

                        $result = array('status' => false, 'message' => lang('msg_something_went_wrong'));
                        $this->response($result, 500);
                    }
                }
            }
        }
        catch (Exception $e)
        {
            // write to system log
            $this->log_model->add('error', $e->getMessage());

            $result = array('status' => false, 'message' => lang('msg_something_went_wrong'));
            $this->response($result, 500);
        }
    }

    protected function user_update()
    {
        try
        {
            // validate the fields
            $run_validation = 'user_update_UI';
            if (defined('ACCESSED_VIA_API') && ACCESSED_VIA_API === TRUE) $run_validation = 'user_update_API';
            $this->form_validation->set_data($this->put());
            if ($this->form_validation->run($run_validation) == FALSE)
            {
                $errors = $this->form_validation->error_array();
                    $result = array('status' => false, 'validation_error' => $errors);
                    $this->response($result, 400);
            }
            else
            {
                // special validations 
                $errors = array();

                if ( ! $this->permissions_model->is_root() && $this->permissions_model->is_root($this->put('user_id')))
                {
                    // write to system log
                    $this->log_model->add('security', 'Unauthorized to modify root user');
                    $errors['email'] = lang('msg_unauthorized');
                }

                 // check ldap_user_id contain domain if not then add
                $this->load->config("ldap");
                $ldap_config = $this->config->item('ldap_config');
                $default = ENVIRONMENT;
                $domain = $ldap_config[$default]['account_suffix'];

                if (strpos($this->put('ldap_user_id'), '@') === false)
                {
                    // user enter only user name 
                    $ldap_user_id = $this->put('ldap_user_id') . $domain;
                }
                else
                {
                    $ldap_user_id = $this->put('ldap_user_id');
                }
                // check if ldap change is permitted (avoid duplicates)
                if ( ! $this->users_model->check_ldap_user_id_change($this->put('user_id'), $ldap_user_id))
                {
                    // write to system log
                    $this->log_model->add('security', 'Ldap user id change not permitted. New ldap user id already exists: ' . $this->put('ldap_user_id'));
                    $errors['email'] = lang('error_ldap_user_id_change_already_exists');
                }

                // check if allowed to assign this role
                $roles = $this->put('roles');
                $auth_role = $this->roles_model->check_role_assign($roles);
                if ($auth_role !== TRUE)
                {
                    // write to system log
                    $this->log_model->add('security', 'Attempted to assign an unauthorized permission role: ' . $auth_role['role']);
                    $errors['roles[]'] = lang('msg_not_authorized_to_assign_role') . ": " . lang($auth_role['role']);
                }

                // avoid assigning a lower role to a user that has been given a higher role by somebody else
                if ($this->roles_model->check_role_assign_to_user($roles, $this->put('user_id')) !== TRUE)
                {
                    // write to system log
                    $this->log_model->add('info', 'Could not assign a lower permission role to user: ' . $this->put('user_id'));
                    $errors['roles[]'] = lang('msg_cannot_assign_lower_role');
                }

                // avoid assigning a lower role to yourself
                if ($this->put('user_id') == $this->users_model->userdata['user_id'])
                {
                    if (!$this->roles_model->check_role_assign_to_myself($roles)) $roles = FALSE;
                }

                // check if allowed to assign groups
                $groups = $this->put('groups');
                $auth_group = $this->groups_model->check_group_assign($groups);
                if ($auth_group !== TRUE)
                {
                    // write to system log
                    $this->log_model->add('security', 'Attempted to assign an unauthorized group: ' . $auth_group['group']);
                    $errors['roles'] = lang('msg_not_authorized_to_assign_group') . ": " . lang($auth_group['group']);
                }
                $user_functions = $this->put('user_functions');
                if ( ! empty($errors))
                {
                    // return validation error
                    $result = array('status' => false, 'validation_error' => $errors);
                    $this->response($result, 400);
                }
                else
                {
                    // get the user's current data
                    $userdata = $this->users_model->get_user($this->put('user_id'));

                    // get old ldap_user_id (to log ldap_user_id change)
                    $old_ldap_user_id = $userdata['ldap_user_id'];

                    // get Active field (to log activating/deactivating users)
                    $prev_active_setting = $this->users_model->check_user_active($this->put('user_id'));

                    // start saving everything to database
                    $data = $this->form_validation->get_prepped_data($this->put());
                    $data['ldap_user_id'] = $ldap_user_id;
                    // save flag to know if we need to send email
                    $send_access_details = element('send_access_details', $data);
                    if ($send_access_details == '1')
                    {
                        // reset the password, to be able to send it by email
                        $temporary_password = $this->auth_model->generate_temporary_password();
                        $data['password'] = $temporary_password;
                        // force password reset, as we are sending access details again by email
                        $data['force_reset_password'] = '1';
                    }

                    $user_random_id = $data['user_random_id'];
                    // start database transaction
                    $this->db->trans_start();
                    
                    // save user
                    unset($data['X-API-KEY']);
                    unset($data['roles']);
                    unset($data['groups']);
                    unset($data['user_functions']);
                    unset($data['user_random_id']);
                    unset($data['send_access_details']);
                    $id = $this->users_model->user_save($this->put('user_id'), $data);

                    // assign roles
                    if ($roles !== FALSE) $this->roles_model->assign_roles($id, $roles);

                    // assign groups
                    if ($groups !== FALSE) $this->groups_model->assign_groups($id, $groups);

                    // assign user functions
                    if ($user_functions !== FALSE) $this->user_functions_model->assign_users_user_functions($id, $user_functions);

                    $this->db->trans_complete();

                    if ($this->db->trans_status())
                    {
                        // check if ldap_user_id has changed, and log it
                        $ldap_user_id_changed = '';
                        if ($data['ldap_user_id'] != $old_ldap_user_id) $ldap_user_id_changed = $old_ldap_user_id . ' -> ' . $data['ldap_user_id'];

                        // write to system log
                        if (empty($ldap_user_id_changed)) $this->log_model->add('write', 'User updated: ' . $data['ldap_user_id'] . ' (' . $id . ')');
                        else $this->log_model->add('write', 'User updated and ldap user id changed: ' . $ldap_user_id_changed . ' (' . $id . ')');

                        // write additional log if user activated or deactivated
                        if ($prev_active_setting != $data['active'] && $data['active'] == '1')
                            $this->log_model->add('info', 'User activated: ' . $data['ldap_user_id'] . ' (' . $id . ')');
                        elseif ($prev_active_setting != $data['active'] && $data['active'] == '0')
                            $this->log_model->add('info', 'User deactivated: ' . $data['ldap_user_id'] . ' (' . $id . ')');

                        $message = lang('msg_user_updated');

                        // send email with access details?
                        if ($send_access_details == '1')
                        {
                            if ($this->email_notifications_model->send_access_details($id, $temporary_password))
                            {
                                // write to system log
                                $this->log_model->add('info', 'Email with access details sent to: ' . $data['email'] . ' (' . $id . ')');
                                $message .= ' ' . lang('msg_email_sent');
                            }
                            else
                            {
                                // write to system log
                                $this->log_model->add('error', 'Could not send email with access details to: ' . $data['email'] . ' (' . $id . ')');
                                $message .= ' ' . lang('msg_email_not_sent');
                            }
                        }
                            $result = array('status' => true, 'id' => $user_random_id, 'message' => $message);
                            $this->response($result, 201);
                    }
                    else
                    {
                            // write to system log
                            $this->log_model->add('error', 'Could not update user: ' . $data['ldap_user_id'] . ' (' . $id . ')');

                            $result = array('status' => false, 'message' => lang('msg_something_went_wrong'));
                            $this->response($result, 500);
                    }
                }
            }
        }   
        catch (Exception $e)
        {
            // write to system log
            $this->log_model->add('error', $e->getMessage());

            $result = array('status' => false, 'message' => lang('msg_something_went_wrong'));
            $this->response($result, 500);
        }
    }

    protected function user_delete()
    {
        try
        {
            // get the id(s) to delete
            $id = $this->delete('id');

            // convert to array
            if ( ! is_array($id)) $id = array($id);

            // validate the fields
            $this->form_validation->set_data($this->delete());
            if ($this->form_validation->run('user_delete_UI') == FALSE)
            {
                $errors = $this->form_validation->error_array();
                $result = array('status' => false, 'validation_error' => $errors);
                $this->response($result, 400);
            }
            else
            {
                // delete users one by one
                $deleted = 0;

                foreach ($id as $user_id)
                {
                    if ( ! $this->permissions_model->is_root() && $this->permissions_model->is_root($user_id))
                    {
                        // write to system log
                        $this->log_model->add('security', 'Unauthorized to delete root user');
                        continue;
                    }

                    // get the user's ldap user id address for logging purposes
                    $ldap_user_id = $this->users_model->get_user_ldap_user_id($user_id);

                    // delete user from database
                    if ($this->users_model->user_delete($user_id))
                    {
                        $this->log_model->add('delete', "User deleted: $ldap_user_id ($user_id)");
                        $deleted++;
                    }
                    else
                    {
                        $this->log_model->add('error', "Could not delete user: $ldap_user_id ($user_id)");
                    }
                }

                $result = array('status' => true, 'message' => sprintf(lang('msg_deleted_x_of_x_users'), $deleted, count($id)));
                $this->response($result, 200);
            }
        }
        catch (Exception $e)
        {
            // write to system log
            $this->log_model->add('error', $e->getMessage());

            $result = array('status' => false, 'message' => lang('msg_something_went_wrong'));
            $this->response($result, 500);
        }
    }

    protected function users_import()
    {
        // load config file
        $this->config->load('files', TRUE);
        $this->config->load('users_import', TRUE);

        // load config arrays
        $files_config = $this->config->item('files');
        $users_import_config = $this->config->item('users_import_UI');

        $filedata = array();

        try
        {
            // validate the fields
            $this->form_validation->set_data($this->post());
            if ($this->form_validation->run('users_import') == FALSE)
            {
                $errors = $this->form_validation->error_array();
                $result = array('status' => false, 'validation_error' => $errors);
                $this->response($result, 400);
            }
            else
            {
                // special validations
                $errors = array();

                // get form data
                $overwrite_contact_details = $this->post('overwrite_contact_details');
                $send_access_details_new = $this->post('send_access_details_new');
                $send_access_details_existing = $this->post('send_access_details_existing');
                $send_access_details = FALSE; // this flag gets set later

                // initialize and check uploaded file
                $config['upload_path'] = $files_config['general_upload_dir'];
                $config['allowed_types'] = 'xlsx';
                $config['max_size']	= '10240'; // 10mb

                $this->load->library('upload', $config);
                if ( ! $this->upload->do_upload('excel_file'))
                {
                    $errors['excel_file'] = $this->upload->display_errors();
                }
                else
                {
                    $filedata = $this->upload->data();

                    // try to open excel file and verify content
                    $ret = $this->users_import_check_excel_file($filedata['full_path']);

                    if ($ret !== TRUE)
                    {
                        $errors = $ret;

                        // slice the array, don't show too many
                        if (count($errors) > 50)
                        {
                            $errors = array_slice($errors, 0, 50);
                            $errors = array_merge($errors, array('more_errors' => lang('etc')));
                        }

                        if ( ! empty($errors)) $errors = array_merge(array('', lang('msg_import_errors_found').'<br />'), $errors);
                    }
                }

                if ( ! empty($errors))
                {
                    // delete uploaded file
                    @unlink($filedata['full_path']);

                    // return validation error
                    $result = array('status' => false, 'validation_error' => $errors);
                    $this->response($result, 400);
                }
                else
                {
                    // start parsing file (again)
                    $xlsx = new SimpleXLSX($filedata['full_path']);

                    $count = 0;
                    $updated = 0;
                    $created = 0;
                    $errors = array();
                    foreach ($xlsx->rows() as $row)
                    {
                        // skip first line of excel
                        $count++;
                        if ($count == 1) continue;

                        // create multi-dimensional array with cell data
                        $line = array();
                        for ($i=0; $i<count($row); $i++)
                        {
                            $line[$users_import_config['columns'][$i]] = $row[$i];
                        }

                        // do some conversions
                        if (strtoupper($line['state_code']) == 'NON-US') $line['state_code'] = '';

                        // get the role data desired permission role
                        $roledata = $this->roles_model->get_role_by_name($line['permission_role']);

                        // run the line through validation (again) to clean up the data
                        $this->form_validation->reset_validation();
                        $this->form_validation->set_rules($users_import_config['validation_rules']);
                        $this->form_validation->set_data($line);
                        $this->form_validation->run();
                        $line = $this->form_validation->get_prepped_data($line);

                        // check if user already exists
                        $updating = FALSE;
                        $user_id = $this->users_model->check_email_exists($line['email']);
                        if ($user_id)
                        {
                            $updating = TRUE;
                            $userdata = $this->users_model->get_user($user_id);
                        }

                        // set correct flag for later
                        if ( ! $updating && $send_access_details_new == '1') $send_access_details = '1';
                        elseif ($updating && $send_access_details_existing == '1') $send_access_details = '1';

                        // create data array
                        $data = array();
                        if ( ! $updating || $overwrite_contact_details == '1')
                        {
                            foreach ($line as $key => $val)
                            {
                                // do not add permission permission_role field to data array, this is assigned differently
                                if ($key == 'permission_role') continue;

                                $data[$key] = $val;
                            }

                            // for new users, set some additional fields
                            if ( ! $updating)
                            {
                                // activate the user
                                $data['active'] = '1';
                            }
                        }

                        if ($send_access_details == '1')
                        {
                            // reset the password, to be able to send it by email
                            $temporary_password = $this->auth_model->generate_temporary_password();
                            $data['password'] = $temporary_password;

                            // require password reset when user first logs in
                            $data['force_reset_password'] = '1';
                        }

                        // create or update user
                        $user_id = $this->users_model->user_save($user_id, $data);
                        if ($user_id)
                        {
                            if ($updating)
                            {
                                $updated++;

                                // write to system log
                                $this->log_model->add('import', 'User updated: ' . $line['email'] . ' (user_id=' . $user_id . ')');
                            }
                            else
                            {
                                $created++;

                                // write to system log
                                $this->log_model->add('import', 'New user created: ' . $line['email'] . ' (user_id=' . $user_id . ')');
                            }
                        }
                        else
                        {
                            if ($updating) $errors[] = lang('msg_error_updating_user') . " (Line $count)";
                            else $errors[] = lang('msg_error_creating_user') . " (Line $count)";
                        }

                        // if we could not save to database, continue to the next line
                        if ( ! $user_id) continue;

                        // assign roles
                        if ($roledata) $this->roles_model->assign_roles($user_id, array($roledata['role_id']));

                        // send email with access details?
                        if ($send_access_details == '1')
                        {
                            // try sending the email
                            if ($this->email_notifications_model->send_access_details($user_id, $temporary_password))
                            {
                                // write to system log
                                $this->log_model->add('info', 'Email with access details sent to: ' . $line['email'] . ' (' . $user_id . ')');
                            }
                            else
                            {
                                // write to system log
                                $this->log_model->add('error', 'Could not send email with access details to: ' . $line['email'] . ' (' . $user_id . ')');

                                $errors[] = lang('line') . " $count: " . lang('msg_error_sending_access_details_email') . $line['email'];
                            }
                        }
                    }

                    // delete uploaded file
                    @unlink($filedata['full_path']);

                    if ( ! empty($errors))
                    {
                        // write to system log
                        $this->log_model->add('import', "Errors occurred during import: \n" . implode("\n", $errors));
                    }

                    $message = lang('msg_import_result') . "<br /><br />\n";
                    $message .= lang('created') . ": $created<br />\n" . lang('updated') . ": $updated<br />\n";
                    if ( ! empty($errors)) $message .= lang('errors') . ': <br />' . implode("<br />\n", $errors);

                    $result = array('status' => true, 'created' => $created, 'updated' => $updated, 'errors' => $errors, 'message' => $message);
                    $this->response($result, 201);
                }
            }
        }
        catch (Exception $e)
        {
            // delete uploaded file
            @unlink(element('full_path', $filedata));

            // write to system log
            $this->log_model->add('error', $e->getMessage());

            $result = array('status' => false, 'message' => lang('msg_something_went_wrong'));
            //$result = array('status' => false, 'message' => $e->getMessage());
            $this->response($result, 500);
        }
    }

	protected function check_permission($module, $action, $dataset=FALSE, $restrictions=FALSE)
	{
		if ( ! $this->permissions_model->check($module, $action, $dataset, $restrictions))
		{
			// write to system log
			$this->log_model->add('security', 'Unauthorized');

			$result = array('status' => false, 'data' => array(), 'message' => lang('msg_unauthorized'));
			$this->response($result, 401);
		}
	}

        
        protected function check_module_operation_access_permission($module, $submodule, $action, $dataset, $user_id = FALSE, $role = false)
	{
		if ( ! $this->permissions_model->check_module_operation_access($module, $submodule, $action, $dataset, $user_id, $role))
		{
			// write to system log
			$this->log_model->add('security', 'Unauthorized');

			$result = array('status' => false, 'data' => array(), 'message' => lang('msg_unauthorized'));
			$this->response($result, 401);
		}
	}

    /*
     * Users Import Check Excel File
     *
     * Reads the data from the Excel file and checks each line to make sure there are no errors.
     *
     * @param string file the full path and file name to read
     * @access private
     * @return array|bool returns true on success or array with errors
     */
    private function users_import_check_excel_file($file)
    {
        // load config array
        $users_import_config = $this->config->item('users_import');

        // read the Excel file
        require_once APPPATH.'third_party/simplexlsx.class.php';
        $xlsx = new SimpleXLSX($file);

        if ( ! $xlsx) return FALSE;

        $errors = array(); // store errors
        $count = 0;
        foreach($xlsx->rows() as $row)
        {
            // skip first line of excel
            $count++;
            if ($count == 1) continue;

            // create multi-dimensional array with cell data
            $line = array();
            for ($i=0; $i<count($users_import_config['columns']); $i++)
            {
                $line[$users_import_config['columns'][$i]] = $row[$i];
            }

            // do some conversions
            if (strtoupper($line['state_code']) == 'NON-US') $line['state_code'] = '';

            // validate this line
            $this->form_validation->reset_validation();
            $this->form_validation->set_rules($users_import_config['validation_rules']);
            $this->form_validation->set_data($line);
            if ($this->form_validation->run() == FALSE)
            {
                $arr = $this->form_validation->error_array();
                foreach ($arr as $key => $val) $errors["$key$count"] = lang('line') . " $count: $val";
            }

            // special validations
            // try to get user_id (if present)
            $user_id = $this->users_model->check_email_exists($line['email']);

            // check state_code
            if ( ! empty($line['state_code']) && ! $this->form_validation->is_valid_state_code($line['state_code'])) $errors["state_code$count"] = lang('line') . " $count: " . lang('msg_invalid_state_code') . " " . $line['state_code'];

            // check country_code
            if ( ! empty($line['country_code']) &&  ! $this->form_validation->is_valid_country_code($line['country_code'])) $errors["country_code$count"] = lang('line') . " $count: " . lang('msg_invalid_country_code') . " " . $line['country_code'];

            // check permission role
            if ( ! empty($line['permission_role']))
            {
                $role = $this->roles_model->get_role_by_name($line['permission_role']);
                if ( ! $role) $errors["permission_role$count"] = lang('line') . " $count: " . lang('msg_invalid_role') . " " . $line['permission_role'];
            }

            // check if allowed to assign permission role
            $auth_role = $this->roles_model->check_role_assign($role['role_id']);
            if ($auth_role !== TRUE) $errors["permission_role$count"] = lang('line') . " $count: " . lang('msg_not_authorized_to_assign_role') . ": " . $line['permission_role'];

            // avoid assigning a lower role to a user that has been given a higher role by somebody else
            if ($user_id && $this->roles_model->check_role_assign_to_user($role['role_id'], $user_id) !== TRUE)
                $errors["permission_role_low$count"] = lang('line') . " $count: " . lang('msg_cannot_assign_lower_role');

            // avoid assigning a lower role to yourself
            if ($user_id == $this->users_model->userdata['user_id'])
            {
                if ( ! $this->roles_model->check_role_assign_to_myself($role['role_id']))
                    $errors["permission_role_self$count"] = lang('line') . " $count: " . lang('msg_cannot_assign_lower_role_to_yourself');
            }

            // check fields depending on permission role
            $crtn = trim($line['crtn']);
            if ($line['permission_role'] == 'site_contact' && empty($crtn))
                $errors["crtn$count"] = lang('line') . " $count: " . lang('msg_site_contact_requires_crtn');
        }

        if ( ! empty($errors)) return $errors;

        return TRUE;
    }
    
    
    
      /** Uploaded file on amazon s3
      * 
      * @param type $filedata
      * @return string
      */
    public function upload_files_to_amazon($filedata,$module,$scope)
    {
        //load files models
        $this->lang->load('files');
        $this->load->model('files_model');
        
        $this->config->load('files', TRUE);
        // load config array
        $files_config = $this->config->item('files');

        $scope_config = $this->files_model->get_scope_config($module,$scope);
        // found required scope config?
        if (!$scope_config)
        {
            // write to system log
            $this->log_model->add('error', "Could not upload file on amazon s3. File scope config not found for scope: $scope");
            return false;
        }
        
        $config = array('module' => $module,'scope' => $scope);
        //load amazon s3 library
        $this->load->library('s3_new', $config);
        
        $s3_uploaded_file = "";
        //image stored at amazon s3
        $actual_image_name = time().$filedata['file_ext'];
        
        $s3_result_arr = $this->s3_new->putObjectFile($filedata['full_path'], $actual_image_name);
        
        if (!empty($s3_result_arr['ETag']) && !empty($s3_result_arr['ObjectURL']))
        {
            $s3_uploaded_file = $s3_result_arr['ObjectURL'];
        }
       
        return $s3_uploaded_file;       
    }
    
    /** Get uploaded file url from amazon s3
     * 
     * @param type $uploaded_file_name
     * @return string
     */
    public function get_uploaded_file_name($uploaded_file_name,$module,$scope)    
    {
        //load files models
        $this->lang->load('files');
        $this->load->model('files_model');
        
        $scope_config = $this->files_model->get_scope_config($module,$scope);
        
        // found required scope config?
        if (!$scope_config)
        {
            // write to system log
            $this->log_model->add('error', "Could not get file from amazon s3. File scope config not found for scope: $scope");
            return false;
        }

        
        $bucket_name = $scope_config['amazon_s3_bucket'];
        $amazon_url = sprintf($scope_config['amazon_url'], $bucket_name);
        $amazon_uploaded_file_name = $amazon_url.$uploaded_file_name;
        return $amazon_uploaded_file_name;
    }

    /** Generate pdf from doc template
     * 
     * @param integer $entity_id
     * @param string $entity
     * @param string $filename
     *
     * @return string
     */
    public function generate_pdf_from_entity_data($entity_id, $entity, $scope, $filename){
        
        
        if (empty($entity_id) || empty($entity) || empty($filename) || empty($scope)) 
        return false;

        $this->load->model('files_model');

//        $scope_config = $this->files_model->get_scope_config($entity,$scope);
        $scope_config = $this->files_model->get_scope_config(MODULE_DOCUMENTS,SUBMODULE_DOCUMENTS_DOCUMENT_TYPES);
       
        if (empty($scope_config))
        {
            return false;
        }

        $full_path = $scope_config['root_dir'] . $filename;
        if (!file_exists($full_path))
        {
            return false;
        }

        $fullpath = '';
        $out_scope = '';
        switch ($entity) {
            case MODULE_EVENTS:
                $fullpath = $this->update_event_template($entity_id, $scope, $filename);
                $this->load->model('events_model');
                $out_scope = $this->events_model->_events_files_scope;
                break;
            case MODULE_CAPAS:
                $fullpath = $this->update_capa_template($entity_id, $scope, $filename);
                $this->load->model('capas_model');
                $out_scope = $this->capas_model->_capas_files_scope;
                break;
            case MODULE_CHANGE_CONTROL:
                $fullpath = $this->update_change_request_template($entity_id, $scope, $filename);
                $this->load->model('change_requests_model');
                $out_scope = $this->change_requests_model->_change_request_files_scope;
                break;
        }
        
        $this->load->config('oscommands');
        $pdf_conversion_command = $this->config->item('pdf_conversion_command');
        $pdf_conversion_command = str_replace('<SOURCE_FILE_FULL_PATH>', $fullpath, $pdf_conversion_command);

        $out_scope_config = $this->files_model->get_scope_config($entity,$out_scope);
        $pdf_conversion_command = str_replace('<OUT_DIRECTORY>', $out_scope_config['root_dir'], $pdf_conversion_command);
        
        $command_status = exec($pdf_conversion_command);

        $temp_file = $scope_config['root_dir'] . TEMP_FILE_PREFIX . "_" . $filename;
        if (file_exists($temp_file))
        {
            unlink($temp_file);
        }
            
        return $command_status;       
    }
    
    /** Convert doc to pdf by id and scope
     * 
     * @param integer $entity_id
     * @param string $scope
     * @return string
     */
    public function convert_doc_to_pdf_by_id_scope($entity_id,$module,$scope,$update_document_variable = false)    
    {
        if (empty($entity_id) || empty($module) || empty($scope)) return false;
        
        $this->load->model('files_model');
        $this->load->model('documents_model');

        $this->config->load('files', TRUE);

        // load config arrays
        $files_config = $this->config->item('files');

        
        $scope_config = $this->files_model->get_scope_config($module,$scope);
        
        // found required scope config?
        if (empty($scope_config))
        {
            return false;
        }

        $documentdata = $this->documents_model->get_document($entity_id);
        if (empty($documentdata['filename']))
        {
            return false;
        }
        
        $fullpath = $scope_config['root_dir']. $documentdata['filename'];
      
        if (!file_exists($fullpath))
        {
            return false;
        }
        
        
        $is_fullpath_have_status_data = false;
        //update document template variable
        if($update_document_variable)
        {
            //update document variable if document mime type "application/msword"
            if($documentdata['extension'] == ".doc" || $documentdata['extension'] == ".docx")
            {
                $fullpath = $this->update_document_template($entity_id , SUBMODULE_DOCUMENTS_DOCUMENTS);
                $is_fullpath_have_status_data = true;
            }
        }
      
        if ($documentdata['enable_pdf_conversion'])
        {
            // we cannot pass output filename when convert doc to pdf
            // so first when we create pdf save file on temp location then move in original location with prefix status
            // load config file

            $this->load->config('oscommands');
            $pdf_conversion_command = $this->config->item('pdf_conversion_command');
            $pdf_conversion_command = str_replace('<SOURCE_FILE_FULL_PATH>', $fullpath, $pdf_conversion_command);
            if($is_fullpath_have_status_data)
            {
                  $pdf_conversion_command = str_replace('<OUT_DIRECTORY>', $scope_config['root_dir'], $pdf_conversion_command);
                  $command_status = exec($pdf_conversion_command);

            }
            else
            {
                  $pdf_conversion_command = str_replace('<OUT_DIRECTORY>', $files_config['general_upload_dir'], $pdf_conversion_command);
                  $temp_file_path = $files_config['general_upload_dir'].$files_config['general_upload_dir']."document_file_id_{$documentdata['document_random_id']}.pdf";
                  $command_status = exec($pdf_conversion_command);

                  // now move pdf file in original place with status prefix
                  $filename_with_status = $this->entities_model->add_entity_status_as_prefix_in_filename(MODULE_DOCUMENTS,SUBMODULE_DOCUMENTS_DOCUMENTS,$documentdata,".pdf");
                  rename($temp_file_path,$scope_config['root_dir'].$filename_with_status['filename_with_extension']);
            }  
            
            if($update_document_variable)
            {
                $prefix_data = $this->entities_model->add_entity_status_as_prefix_in_filename(MODULE_DOCUMENTS,SUBMODULE_DOCUMENTS_DOCUMENTS,$documentdata);
                $file_name = $scope_config['root_dir'].TEMP_FILE_PREFIX."_".$prefix_data['filename_with_extension'];
        
                if (file_exists($file_name))
                {
                    unlink($file_name);
                }
            }
            return $command_status;
        }
        else
        {
            return true;
        }
    }
    
    public function update_document_template($entity_id, $entity)
    {        
        ini_set('max_execution_time', -1);
        
        include(APPPATH.'libraries/PHPWord-develop/src/PhpWord/Settings.php');
        include(APPPATH.'libraries/PHPWord-develop/src/PhpWord/Shared/ZipArchive.php');
        include(APPPATH.'libraries/PHPWord-develop/vendor/zendframework/zend-stdlib/src/StringUtils.php');
        include(APPPATH.'libraries/PHPWord-develop/src/PhpWord/TemplateProcessor.php');
        
        $this->load->model('files_model');
        $this->load->model('document_types_model');
        $this->load->model('documents_model');
        
        switch ($entity)
        {
            case SUBMODULE_DOCUMENTS_DOCUMENTS:
                
                $document_id = $entity_id;
                
                //get document author,reviewer, approver ,qa data
                $authorsdata = $this->entities_model->get_entity_users_by_role($document_id, ROLE_APPROVERS, SUBMODULE_DOCUMENTS_DOCUMENTS);
                $approversdata = $this->entities_model->get_entity_users_by_role($document_id, ROLE_APPROVERS, SUBMODULE_DOCUMENTS_DOCUMENTS);
                $reviewersdata = $this->entities_model->get_entity_users_by_role($document_id, ROLE_REVIEWERS, SUBMODULE_DOCUMENTS_DOCUMENTS);
                $qadata = $this->entities_model->get_entity_users_by_role($document_id, ROLE_QA, SUBMODULE_DOCUMENTS_DOCUMENTS);
                $employeesdata = $this->entities_model->get_entity_users_by_role($document_id, ROLE_EMPLOYEES, SUBMODULE_DOCUMENTS_DOCUMENTS);
                $data = $this->documents_model->get_document($document_id);
                
                $filename = $data['filename'];
                
                $scope_config = $this->files_model->get_scope_config(MODULE_DOCUMENTS,SUBMODULE_DOCUMENTS_DOCUMENTS);
                
                break;
        }
        
        if (empty($filename))
        {
            return false;
        }

        // found required scope config?
        if (empty($scope_config))
        {
            return false;
        }

        $fullpath = $scope_config['root_dir']. $filename;

        if (!file_exists($fullpath))
        {
            return false;
        }
        
        $document_version = $data['document_latest_version'];
        
        $this->load->model('users_model');
        // get all users
        $users = $this->users_model->get_all_users_arr();
        
        
        $qa_signed_users = $this->entities_model->get_entity_signed_users_details($document_id, ROLE_QA, SUBMODULE_DOCUMENTS_DOCUMENTS);
        
        $approver_signed_users = $this->entities_model->get_entity_signed_users_details($document_id, ROLE_APPROVERS, SUBMODULE_DOCUMENTS_DOCUMENTS);
        
        $author_signed_users = $this->entities_model->get_entity_signed_users_details($document_id, ROLE_AUTHORS, SUBMODULE_DOCUMENTS_DOCUMENTS);
        
        //load template file
        $document = new \PhpOffice\PhpWord\TemplateProcessor($fullpath);
        
        //update author data
        if(!empty($authorsdata))
        {
            $i = 1;
            foreach($authorsdata as $idx => $author)
            {
               $author_const = "Author".$i;                 
               $document->setValue($author_const, $users[$author]);
               
               //There is no case insensitive replacement, so trying few combination
               $author_const = "author".$i;                 
               $document->setValue($author_const, $users[$author]);
               $author_const = "AUTHOR".$i;                 
               $document->setValue($author_const, $users[$author]);
               
               
               $author_date_const = "Author".$i."_date_signed";
               $author_signed_date = $this->localization_model->timestamp2datetime($author_signed_users[$idx]['date_created']);
               $document->setValue($author_date_const, $author_signed_date);
               
               //There is no case insensitive replacement, so trying few combination
               $author_date_const = "author".$i."_date_signed";
               $document->setValue($author_date_const, $author_signed_date);
               $author_date_const = "AUTHOR".$i."_DATE_SIGNED";
               $document->setValue($author_date_const, $author_signed_date);
               
               $i++;
            }
            
            if ($i < MAX_TEMP_VALUE_COUNT)
            {
                for ($j = $i; $j <=MAX_TEMP_VALUE_COUNT; $j++)
                {
                    $author_const = "Author".$j;
                    $document->setValue($author_const, '');
                    $author_const = "author".$i;                 
                    $document->setValue($author_const, '');
                    $author_const = "AUTHOR".$i;                 
                    $document->setValue($author_const, '');
                    
                    $author_date_const = "Author".$i."_date_signed";
                    $document->setValue($author_date_const, '');
                    $author_date_const = "author".$i."_date_signed";
                    $document->setValue($author_date_const, '');
                    $author_date_const = "AUTHOR".$i."_DATE_SIGNED";
                    $document->setValue($author_date_const, '');
                }
            }
        }
        
        //update reviewer data
        if(!empty($reviewersdata))
        {
            $i = 1;
            foreach($reviewersdata as $idx => $reviewer)
            {
               $reviewer_const = "Reviewer".$i;                
               $document->setValue($reviewer_const, $users[$reviewer]);
               
               $reviewer_const = "reviewer".$i;                
               $document->setValue($reviewer_const, $users[$reviewer]);
               $reviewer_const = "REVIEWER".$i;                
               $document->setValue($reviewer_const, $users[$reviewer]);
               
               $i++;
            }
            
            if ($i < MAX_TEMP_VALUE_COUNT)
            {
                for ($j = $i; $j <=MAX_TEMP_VALUE_COUNT; $j++)
                {
                    $reviewer_const = "Reviewer".$j;                
                    $document->setValue($reviewer_const, '');
                    $reviewer_const = "reviewer".$j;                
                    $document->setValue($reviewer_const, '');
                    $reviewer_const = "REVIEWER".$j;                
                    $document->setValue($reviewer_const, '');
                }
            }
        }
        
        //update approver data
        if(!empty($approversdata))
        {
            $i = 1;
            foreach($approversdata as $idx => $approver)
            {
               $approver_const = "Approver".$i;               
               $document->setValue($approver_const, $users[$approver]);
               
               $approver_const = "approver".$i;               
               $document->setValue($approver_const, $users[$approver]);
               $approver_const = "APPROVER".$i;               
               $document->setValue($approver_const, $users[$approver]);
               
               
               $approver_date_const = "Approver".$i."_date_signed";
               $approver_signed_date = $this->localization_model->timestamp2datetime($approver_signed_users[$idx]['date_created']);
               $document->setValue($approver_date_const, $approver_signed_date);
               
               $approver_date_const = "approver".$i."_date_signed";
               $document->setValue($approver_date_const, $approver_signed_date);
               $approver_date_const = "APPROVER".$i."_DATE_SIGNED";
               $document->setValue($approver_date_const, $approver_signed_date);
               
               $i++;
            }
            
            if ($i < MAX_TEMP_VALUE_COUNT)
            {
                for ($j = $i; $j <=MAX_TEMP_VALUE_COUNT; $j++)
                {
                    $approver_const = "Approver".$j;
                    $document->setValue($approver_const, '');
                    $approver_const = "approver".$j;
                    $document->setValue($approver_const, '');
                    $approver_const = "APPROVER".$j;
                    $document->setValue($approver_const, '');
                    
                    $approver_date_const = "Approver".$i."_date_signed";
                    $document->setValue($approver_date_const, '');
                    $approver_date_const = "approver".$i."_date_signed";
                    $document->setValue($approver_date_const, '');
                    $approver_date_const = "APPROVER".$i."_DATE_SIGNED";
                    $document->setValue($approver_date_const, '');
                }
            }
        }
        //update qa data
        if(!empty($qadata))
        {
            $i = 1;
            foreach($qadata as $idx => $qa)
            {
               $qa_const = "QA".$i;         
               $document->setValue($qa_const, $users[$qa]);
               $qa_const = "Qa".$i;         
               $document->setValue($qa_const, $users[$qa]);
               $qa_const = "qa".$i;         
               $document->setValue($qa_const, $users[$qa]);
               
               $qa_date_const = "QA".$i."_date_signed";
               $qa_signed_date = $this->localization_model->timestamp2datetime($qa_signed_users[$idx]['date_created']);
               $document->setValue($qa_date_const, $qa_signed_date);
               $qa_date_const = "Qa".$i."_date_signed";
               $document->setValue($qa_date_const, $qa_signed_date);
               $qa_date_const = "qa".$i."_date_signed";
               $document->setValue($qa_date_const, $qa_signed_date);
               
               $i++;
            }
            
            if ($i < MAX_TEMP_VALUE_COUNT)
            {
                for ($j = $i; $j <=MAX_TEMP_VALUE_COUNT;$j++)
                {
                    $qa_const = "QA".$j;               
                    $document->setValue($qa_const, '');
                    $qa_const = "Qa".$j;               
                    $document->setValue($qa_const, '');
                    $qa_const = "qa".$j;               
                    $document->setValue($qa_const, '');
                    
                    $qa_date_const = "QA".$i."_date_signed";
                    $document->setValue($qa_date_const, '');
                    $qa_date_const = "Qa".$i."_date_signed";
                    $document->setValue($qa_date_const, '');
                    $qa_date_const = "qa".$i."_date_signed";
                    $document->setValue($qa_date_const, '');
                    $qa_date_const = "QA".$i."_DATE_SIGNED";
                    $document->setValue($qa_date_const, '');
                }
            }
        }
        
        //update version
        $document->setValue('Version', $document_version);
        $document->setValue('VERSION', $document_version);
        $document->setValue('version', $document_version);
        
        //update version
        $document->setValue('DocumentId', $data['document_unique_id']);
        $document->setValue('DocumentID', $data['document_unique_id']);
        $document->setValue('DOCUMENTID', $data['document_unique_id']);
        $document->setValue('documentid', $data['document_unique_id']);
        
        //get scope config
        $scope = $this->documents_model->_document_files_scope;
        $scope_config = $this->files_model->get_scope_config(MODULE_DOCUMENTS,$scope);
        
        $prefix_data = $this->entities_model->add_entity_status_as_prefix_in_filename(MODULE_DOCUMENTS,SUBMODULE_DOCUMENTS_DOCUMENTS,$data);
        $file_name = $scope_config['root_dir'].TEMP_FILE_PREFIX."_".$prefix_data['filename_with_extension'];
        //save updated document
        $document->saveAs($file_name);
        return $file_name;
    }
    
    public function update_event_template($event_id, $scope, $filename)
    {        
        ini_set('max_execution_time', -1);
        
        include(APPPATH.'libraries/PHPWord-develop/src/PhpWord/Settings.php');
        include(APPPATH.'libraries/PHPWord-develop/src/PhpWord/Shared/ZipArchive.php');
        include(APPPATH.'libraries/PHPWord-develop/vendor/zendframework/zend-stdlib/src/StringUtils.php');
        include(APPPATH.'libraries/PHPWord-develop/src/PhpWord/TemplateProcessor.php');
        
        $this->load->model('files_model');
        $this->load->model('document_types_model');
        $this->load->model('event_types_model');
        $this->load->model('impact_levels_model');
        $this->load->model('events_model');
        $this->load->model('modules_roles_model'); 
        $this->load->model('entities_model');
        
        $data = $this->events_model->get_event($event_id);
                
        //$investigators = $this->events_model->get_users($event_id, ROLE_INVESTIGATORS);
        //$qadata = $this->events_model->get_users($event_id, ROLE_QA_EVENTS);
        $qadata = $this->entities_model->get_entity_users_by_role($event_id, ROLE_QA_EVENTS, SUBMODULE_EVENTS_EVENTS);
        $investigators = $this->entities_model->get_entity_users_by_role($event_id, ROLE_INVESTIGATORS, SUBMODULE_EVENTS_EVENTS);

        $scope_config = $this->files_model->get_scope_config(MODULE_DOCUMENTS,SUBMODULE_DOCUMENTS_DOCUMENT_TYPES);
               
        if (empty($filename))
        {
            return false;
        }

        // found required scope config?
        if (empty($scope_config))
        {
            return false;
        }

        $fullpath = $scope_config['root_dir'] . $filename;

        if (!file_exists($fullpath))
        {
            return false;
        }
        
        $this->load->model('users_model');
        // get current user
        $user = $this->users_model->userdata;
        
        //load template file
        $document = new \PhpOffice\PhpWord\TemplateProcessor($fullpath);

        $event_type = $this->event_types_model->get_event_type($data['event_type_id']);
        $impact_level = $this->impact_levels_model->get_impact_level($data['impact_level_id']);
        
        $replaceString = lang('title_event_details');
        $replaceString .= '\n' . lang('event_title') . ': ' . $data['event_title'];
        $replaceString .= '\n' . lang('event_type') . ': ' . $event_type['event_type'];
        $replaceString .= '\n' . lang('event_impact_level') . ': ' . $impact_level['impact_level'];
        $replaceString .= '\n' . lang('event_status') . ': ' . lang($data['event_status']);
        $replaceString .= '\n' . lang('event_date') . ': ' . date("d-m-Y", strtotime($data['event_date']));

        $replaceString .= '\n\n' . $user['firstname'] . ' ' . $user['lastname'] . '\n';
        
        $roleString = '';
        if($user['user_id'] == $data['user_created']){
            $roleString .= 'Initiator';
        }
        if($user['user_id'] == $data['responsible_person_id']){
            if($roleString != ''){
                $roleString .= ', ';
            }
            $roleString .= 'Responsible Person';
        }
        if(in_array($user['user_id'], $investigators)){
            if($roleString != ''){
                $roleString .= ', ';
            }
            $roleString .= 'Investigator';
        }
        if(in_array($user['user_id'], $qadata)){
            if($roleString != ''){
                $roleString .= ', ';
            }
            $roleString .= 'QA Person';
        }
        if($roleString != ''){
            $replaceString .= $roleString . '\n';
        }

        $currentDateTime = $this->localization_model->timestamp2datetime(now());
        $replaceString .= $currentDateTime;
        
        $replaceString = str_replace("\\n","</w:t><w:br/><w:t>",$replaceString);
        $document->setValue('event_data', $replaceString);
        
        $file_name = $scope_config['root_dir'].TEMP_FILE_PREFIX."_".$filename;
        //save updated document
        $document->saveAs($file_name);
        return $file_name;
    }

    public function update_capa_template($capa_id, $scope, $filename)
    {        
        ini_set('max_execution_time', -1);
        
        include(APPPATH.'libraries/PHPWord-develop/src/PhpWord/Settings.php');
        include(APPPATH.'libraries/PHPWord-develop/src/PhpWord/Shared/ZipArchive.php');
        include(APPPATH.'libraries/PHPWord-develop/vendor/zendframework/zend-stdlib/src/StringUtils.php');
        include(APPPATH.'libraries/PHPWord-develop/src/PhpWord/TemplateProcessor.php');
        
        $this->load->model('files_model');
        $this->load->model('document_types_model');
        $this->load->model('capas_model');
        $this->load->model('modules_roles_model');
        $this->load->model('entities_model');
        
        $data = $this->capas_model->get_capa($capa_id);
        
        //$qadata = $this->capas_model->get_users($capa_id, ROLE_QA_CAPA);
        $qadata = $this->entities_model->get_entity_users_by_role($capa_id, ROLE_QA_CAPA, SUBMODULE_CAPAS_CAPAS);

        $scope_config = $this->files_model->get_scope_config(MODULE_DOCUMENTS,SUBMODULE_DOCUMENTS_DOCUMENT_TYPES);
        
        if (empty($filename))
        {
            return false;
        }

        // found required scope config?
        if (empty($scope_config))
        {
            return false;
        }

        $fullpath = $scope_config['root_dir'] . $filename;

        if (!file_exists($fullpath))
        {
            return false;
        }
        
        $this->load->model('users_model');
        // get current user
        $user = $this->users_model->userdata;
        
        //load template file
        $document = new \PhpOffice\PhpWord\TemplateProcessor($fullpath);

        $replaceString = lang('title_capa_details');
        $replaceString .= '\n' . lang('capa_title') . ': ' . $data['capa_title'];
        $replaceString .= '\n' . lang('linked_to') . ': ' . $data['event_title'];
        $replaceString .= '\n' . lang('group') . ': ' . $data['group'];
        $replaceString .= '\n' . lang('status') . ': ' . lang($data['status']);
        
        $replaceString .= '\n\n' . $user['firstname'] . ' ' . $user['lastname'] . '\n';

        $roleString = '';
        if($user['user_id'] == $data['user_created']){
            $roleString .= 'Initiator';
        }
        if($user['user_id'] == $data['responsible_person_id']){
            if($roleString != ''){
                $roleString .= ', ';
            }
            $roleString .= 'Responsible Person';
        }
        if(in_array($user['user_id'], $qadata)){
            if($roleString != ''){
                $roleString .= ', ';
            }
            $roleString .= 'QA Person';
        }
        if($roleString != ''){
            $replaceString .= $roleString . '\n';
        }        
 
        $currentDateTime = $this->localization_model->timestamp2datetime(now());
        $replaceString .= $currentDateTime;
        
        $replaceString = str_replace("\\n","</w:t><w:br/><w:t>",$replaceString);
        $document->setValue('capa_data', $replaceString);
        
        $file_name = $scope_config['root_dir'].TEMP_FILE_PREFIX."_".$filename;
        //save updated document
        $document->saveAs($file_name);
        return $file_name;
    }
    
    public function update_change_request_template($change_request_id, $scope, $filename)
    {        
        ini_set('max_execution_time', -1);
        
        include(APPPATH.'libraries/PHPWord-develop/src/PhpWord/Settings.php');
        include(APPPATH.'libraries/PHPWord-develop/src/PhpWord/Shared/ZipArchive.php');
        include(APPPATH.'libraries/PHPWord-develop/vendor/zendframework/zend-stdlib/src/StringUtils.php');
        include(APPPATH.'libraries/PHPWord-develop/src/PhpWord/TemplateProcessor.php');
        
        $this->load->model('files_model');
        $this->load->model('documents_model');
        $this->load->model('document_types_model');
        $this->load->model('change_requests_model');
        $this->load->model('modules_roles_model');
        
        $data = $this->change_requests_model->get_change_request($change_request_id);
        
        if(!empty($data))
        {
            $taskdata = $this->entities_model->get_entity_tasks(MODULE_CHANGE_CONTROL, SUBMODULE_CHANGE_CONTROL_CHANGE_REQUESTS, $change_request_id);

            if(!empty($taskdata))
            {
                $responsible_users = array();
                foreach ($taskdata as $key => $task)
                {
                    $task_id = $task['entity_task_id'];
                    $responsible_users[$task_id] = $this->entities_model->get_entity_users_by_role($task_id, ROLE_RESPONSIBLE_USERS_CHANGE_CONTROL_CHANGE_REQUESTS_TASK, SUBMODULE_CHANGE_CONTROL_CHANGE_REQUEST_TASKS);
                    $task_file_data_arr = $this->entities_model->get_all_entity_files(MODULE_CHANGE_CONTROL,CHANGE_CONTROL_CHANGE_REQUEST_TASKS_OPEN, $task_id);
                    
                    if (empty($task_file_data_arr))
                    {
                        $task_file_data[$task_id] = array();
                    }
                    else
                    {
                        foreach ($task_file_data_arr as $key => $file_data)
                        {
                            $task_file_data[$task_id] = $file_data;
                        }
                    }
                }
            }
        }

        $change_request_data_arr = array(
          "title" => $data['title'],
          "reason" => $data['reason_for_change'],
          "cost" => $data['cost_estimate'],
          "group" => $data['group'],
          "date_created" => $this->localization_model->timestamp2date($data['date_created']),
          "status" => lang($data["status"])
        );
        
        $task_data_arr = array();
        foreach ($taskdata as $Tkey => $task)
        {   
            $task_id = $task['entity_task_id'];
            $task_data_arr[$Tkey]['title'] = $task['title'];
            $task_data_arr[$Tkey]['description'] = $task['description'];
            $responsible_user_arr = array();
            foreach ($responsible_users[$task_id] as $key => $responsible_user)
            {
                $user_id = $responsible_user;
                $user_data = $this->users_model->get_user($user_id);
                $fullname = $this->users_model->get_full_name($user_data);
                $user_email = $user_data['email'];
                $responsible_user_arr[$key]['fullname'] = $fullname;
                $responsible_user_arr[$key]['email'] = $user_email;
            }
            $task_data_arr[$Tkey]['responsible'] = $responsible_user_arr;
        }
        
        $qa_signed_users = $this->entities_model->get_entity_signed_users_details($change_request_id, ROLE_QA_CHANGE_CONTROL, SUBMODULE_CHANGE_CONTROL_CHANGE_REQUESTS);
        $approver_signed_users = $this->entities_model->get_entity_signed_users_details($change_request_id, ROLE_APPROVERS_CHANGE_CONTROL, SUBMODULE_CHANGE_CONTROL_CHANGE_REQUESTS);
        
        
        $qa_signed_users_detail = array();
        if(!empty($qa_signed_users))
        {
            foreach ($qa_signed_users as $key => $value)
            {
                $user_id = $value['user_id'];
                $user_data = $this->users_model->get_user($user_id);
                $fullname = $this->users_model->get_full_name($user_data);
                
                $qa_signed_users_detail[$key]['email'] = $this->users_model->get_user_email($user_id);
                $qa_signed_users_detail[$key]['fullname'] = $fullname;
                $qa_signed_users_detail[$key]['user_role'] = lang('qa');
                $qa_signed_users_detail[$key]['date'] = $this->localization_model->timestamp2datetime($value['date_created']);
            }
            
        }
        
        
        $approver_signed_users_detail = array();
        if(!empty($approver_signed_users))
        {
            foreach ($approver_signed_users as $key => $value)
            {
                $user_id = $value['user_id'];
                $user_data = $this->users_model->get_user($user_id);
                $fullname = $this->users_model->get_full_name($user_data);
                
                $approver_signed_users_detail[$key]['email'] = $this->users_model->get_user_email($user_id);
                $approver_signed_users_detail[$key]['fullname'] = $fullname;
                $approver_signed_users_detail[$key]['user_role'] = lang('approver');
                $approver_signed_users_detail[$key]['date'] = $this->localization_model->timestamp2datetime($value['date_created']);
            }
            
        }
        
        $digital_signed_users = array(
            'approvers' => $approver_signed_users_detail,
            'qa' => $qa_signed_users_detail
        );
        
        $change_request_details = array(
            "change_request" => $change_request_data_arr,
            "task" => $task_data_arr,
            "digital_signatures" => $digital_signed_users
        );
        
        $scope_config = $this->files_model->get_scope_config(MODULE_DOCUMENTS,SUBMODULE_DOCUMENTS_DOCUMENT_TYPES);
        
        if (empty($filename))
        {
            return false;
        }

        // found required scope config?
        if (empty($scope_config))
        {
            return false;
        }

        $fullpath = $scope_config['root_dir'] . $filename;

        if (!file_exists($fullpath))
        {
            return false;
        }
        
        $this->load->model('users_model');
        // get current user
        $user = $this->users_model->userdata;
        
        //load template file
        $document = new \PhpOffice\PhpWord\TemplateProcessor($fullpath);

//        p($change_request_details);
        $replaceString = 'Change Request Details:';
        
        // Change request deatials
        $replaceString .= '\nTitle: ' . $change_request_details['change_request']['title'];
        $replaceString .= '\nReason: ' . $change_request_details['change_request']['reason'];
        $replaceString .= '\nCost: ' . $change_request_details['change_request']['cost'];
        $replaceString .= '\nGroup: ' . $change_request_details['change_request']['group'];
        $replaceString .= '\nDate Created: ' . $change_request_details['change_request']['date_created'];
        $replaceString .= '\nStatus: ' . $change_request_details['change_request']['status'];

        $replaceString .= '\n\n';
        // Task details
        $replaceString .= 'Task Details';
        foreach ($change_request_details['task'] as $key => $tasks_detail)
        {
           $replaceString .= '\nTitle: ' . $tasks_detail['title'];  
           $replaceString .= '\nDescription: ' . $tasks_detail['description'];  
           
           $replaceString .= '\nResponsible Users';
           foreach ($tasks_detail['responsible'] as $key => $responsible_users_details)
           {
                $replaceString .= '\nName: ' . $responsible_users_details['fullname'];   
                $replaceString .= '\nEmail: ' . $responsible_users_details['email'];   
           }
        }
        // Digital Signatures
        if(!empty($change_request_details['digital_signatures']['approvers'] || $change_request_details['digital_signatures']['qa']))
        {
            
            $replaceString .= '\n\n';
            $replaceString .= 'Digital Signatures Details';      
            if(!empty($change_request_details['digital_signatures']['approvers']))
            {
                foreach ($change_request_details['digital_signatures']['approvers'] as $key => $approver_details)
                {
                    $replaceString.= '\nApprover Users';
                    $replaceString .= '\nName: ' . $approver_details['fullname']; 
                    $replaceString .= '\nEmail: ' . $approver_details['email']; 
                    $replaceString .= '\nRole: ' . $approver_details['user_role']; 
                    $replaceString .= '\nDate And Time: ' . $approver_details['date']; 
                }
            }
            if(!empty($change_request_details['digital_signatures']['qa']))
            {
                foreach ($change_request_details['digital_signatures']['qa'] as $key => $qa_details)
                {
                    $replaceString.= '\nQA Users';
                    $replaceString .= '\nName: ' . $qa_details['fullname']; 
                    $replaceString .= '\nEmail: ' . $qa_details['email']; 
                    $replaceString .= '\nRole: ' . $qa_details['user_role']; 
                    $replaceString .= '\nDate And Time: ' . $qa_details['date']; 
                }
            }
        }

        $replaceString = str_replace("\\n","</w:t><w:br/><w:t>",$replaceString);
        $document->setValue('change_request_data',$replaceString);
        
        $file_name = $scope_config['root_dir'].TEMP_FILE_PREFIX."_".$filename;
        //save updated document
        $document->saveAs($file_name);
        return $file_name;
    }

    // generate pdf file from html code
    public function pdf_generate($htmlTemplete,$fileName)
    {
        // load pdf library
        $this->load->library('pdf');
        
        // load html file
        $this->pdf->load_html($htmlTemplete);
        
        // set paper size
        $this->pdf->set_paper('a4', 'portrait');
        
        // render pdf file
        $this->pdf->render();
        
        // download pdf file with fileName
        $this->pdf->stream($fileName);
    }

    /**
     * Upload file to S3
     *
     * @return Mixed download url or false if upload failed
     */
    public function upload_file_to_S3($module,$scope, $file)
    {
        //$this->load->config('files');
            
        //$source_file = base64_decode($file);
        $source_file = $file;
        $raw_file_name = basename($source_file);
         
        $config = array('module'  => $module, 'scope' => $scope);
//        $scopes = $this->config->item('scopes');
        $scope_config = $this->files_model->get_scope_config($module,$scope);
        
          
        if (!empty($scope_config['sync_to_s3']))
        {      
            $this->load->library('S3_new', $config);
                
            $s3_result_arr = $this->s3_new->putObjectFile($source_file, $raw_file_name);

            if (!empty($s3_result_arr['ETag']) && !empty($s3_result_arr['@metadata']['effectiveUri']))
            {    
                $download_url = $s3_result_arr['@metadata']['effectiveUri'];
                $this->log_model->add('info', $module .' >> '.$scope.' >> '. $download_url);
                return $download_url;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    } 

    public function delete_s3_file($module,$scope, $path)
    {
        $this->load->config('files');
        $config = array('module' => $module, 'scope' => $scope);

        $parts = explode('/', $path);
        $target_file = array_pop($parts);

        $this->load->library('S3_new', $config);

        return $this->s3_new->deleteObjectFile($target_file);
    }   
}
